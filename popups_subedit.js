
/**
 * Popups: Subedit behavior
 */


/**
 * Attach the behavior.
 */
Drupal.behaviors.popups_subedit = function(context) {
  if(!$('body').hasClass('popups-subedit-processed')) {
    $('body').addClass('popups-subedit-processed');
    // Bind to the popups API custom form_success event.
    console.log("Binding popups subedit");
    $(document).bind('popups_open_path_done', function(obj, element, href) {
      console.log('popups_open_path_done: ' + href);
      group_class = $(element).attr('group_class');
      console.log('group_class ' + group_class);
//      console.log(b);
      // filter the popup.
      $submit = $('#popups #edit-submit')
      popups_subedit_reveal($submit);
      $group = $('#popups fieldset.' + group_class);
      popups_subedit_reveal($group);
    });
  }
};

function popups_subedit_reveal($element) {
  if ($element.is('form')) {
    return;
  }
  else {
    $element.show();
    $element.addClass('revealed');
    $element.siblings().not('.revealed').hide();
    popups_subedit_reveal($element.parent());
  }
} 